
function fetchApprovalFromQueryToHeaderMapping(props) {
  const spreadsheetTmz = SpreadsheetApp.getActiveSpreadsheet().getSpreadsheetTimeZone();
  const formatDate = (date) => Utilities.formatDate(new Date(date), spreadsheetTmz, 'YYYY-MM-dd')
  let entries = {
    'entry.1804961743': props.emailAddress,
    'entry.215893547': props.supportType, //=What+type+of+support+do+you+need?
    'entry.795101687': props.studyLocation, //=Where+in+the+world+would+the+participants+in+your+study+be+located?
    'entry.632151648': props.participantsNum, //=How+many+participants+do+you+need+for+this+study?
    'entry.625442296': props.studyName, //What's+the+name+of+the+study?
    'entry.44957032': props.otherResearcher, //We've+captured+your+username+already.+Any+other+researchers+working+with+you+on+this+study?
    'entry.322618690': formatDate(props.targetDate), //2021-07-13
    'entry.1539067757': props.typeOfStudy, //Which+type+of+study+are+you+planning+to+do?
    'entry.545388425': props.participantPreference, //If+we+are+unable+to+find+the+participants+you're+looking+for,+would+you+prefer+to:
    'entry.2112183337': props.participantCategory, //Do+your+participants+belong+to+any+of+the+following+categories?
    'entry.1080127653': props.participantType, //What+type+of+participants+are+you+planning+to+use?
    'entry.773706764': props.pcounsel, //If+you+are+planning+to+use+TVCs+or+minors,+who+is+the+pCounsel+who+has+approved+this+study?
    'entry.31662527': props.cityRegionCountryLocation, //In+which+city/region/country+will+study+sessions+take+place?
    'entry.1666382629': props.similarStudy, //If+you+or+another+researcher+received+recruiting+support+for+a+previous+study+with+similar+criteria,+please+let+us+know+the+recruiter,+study+name,+and+approximate+dates.
    'entry.1867002993': props.studySpreadsheetLink, //Paste+the+link+to+your+Study+Spreadsheet+here.+Ensure+that+your+Screener+Form+has+been+linked+to+it+and+that+the+sharing+settings+for+both+are:+private,+with+uxr-operations-share@google.com+added+as+an+editor/collaborator.
    'entry.2017910400': props.researchPlan, //Please+include+a+link+to+your+research+plan+or+any+other+relevant+documentation+that+would+be+helpful+for+this+recruitment+effort.
    'entry.1560391638': props.otherData, //Anything+else+we+should+know?+If+you+will+be+out+of+office+or+on+vacation+before+your+study+begins,+please+indicate+the+dates+and+the+primary+contact+person+while+you're+gone.
    'entry.66166391': props.researcherLocation, //Researcher+Location
    'entry.57935563': props.costCenterCode, //Cost+Center+Code+(recruiters+do+not+edit+this+column)
    'entry.329689144': props.costCenterName, //Cost+Center+Name+(recruiters+do+not+edit+this+column)
    'entry.361059011': props.productArea, //Product+Area+(recruiters+do+not+edit+this+column)
    'entry.79469294': props.supported, //Are+they+on+the+list+of+supported+cost+centers?
    'entry.1138349615': props.spreadsheetId,
    /** removed to pass through blank  */
    // 'entry.1193152887': props.pod, //Pod
    'entry.1703610094': props.buCode, //BU+Code
    'entry.1769140199': props.mangerList, //Manager+List'
    'entry.1414735975': props.unifiedRollup, //unified_rollup_level_1
    'entry.525115626': props.fromLdap, //by+ldap
    'entry.1859611422': props.fromSpreadsheetName, //from+spreadsheet+name
    'entry.1998626020': props.transferReason, //reason+for+transfer
    // 'entry.1688176278': approvalStatus, //Approve || Deny
  }
  return entries
}

function getUrlLink(props) {
  let formId = "1FAIpQLSd37H722wFqXv4Cr9-wQRUfTVF7YGVpTY5TV730oPdl6g1jLw"
  let baseUrl = `https://docs.google.com/forms/d/e/${formId}/viewform?usp=pp_url`
  let url = Object.keys(props).reduce(
    (acc, entry) => {
      let inputValue = props[entry] || ""
      return inputValue === "" ? acc : (acc += `&${entry}=${encodeURIComponent(inputValue)}`)
    },
    baseUrl)
  const approvalStatusEntry = 'entry.1688176278'
  const preFilledUrls = {
    approveUrl: `${url}&${approvalStatusEntry}=Approve`,
    denyUrl: `${url}&${approvalStatusEntry}=Deny`,
  }
  return preFilledUrls
}

/*
 * converts namedValues headers to properties
 * @param Object(event) from form submission
 * @return Object
 */
function fetchSubmissionProps({ namedValues }) {
  const defaultResponse = (question) => [`Question "${question}" Not Found`]
  const {
    'Email Address': emailAddress = defaultResponse('Email Address (from studyqueue)'),
    'What type of support do you need?': supportType = defaultResponse('What type of support do you need?'),
    'Where in the world would the participants in your study be located?': studyLocation = defaultResponse('Where in the world would the participants in your study be located?'),
    'How many participants do you need for this study?': participantsNum = defaultResponse('How many participants do you need for this study?'), //=How+many+participants+do+you+need+for+this+study?
    "What's the name of the study?": studyName = defaultResponse("What's the name of the study?"), //What's+the+name+of+the+study?
    "We've captured your username already. Any other researchers working with you on this study?": otherResearcher = defaultResponse("We've captured your username already. Any other researchers working with you on this study?"), //We've+captured+your+username+already.+Any+other+researchers+working+with+you+on+this+study?
    'What is the targeted first date of the study?': targetDate = defaultResponse('What is the targeted first date of the study?'), //2021-07-13
    'Which type of study are you planning to do?': typeOfStudy = defaultResponse('Which type of study are you planning to do?'), //Which+type+of+study+are+you+planning+to+do?
    "If we are unable to find the participants you're looking for, would you prefer to:": participantPreference = defaultResponse("If we are unable to find the participants you're looking for, would you prefer to:"), //If+we+are+unable+to+find+the+participants+you're+looking+for,+would+you+prefer+to:
    'Do your participants belong to any of the following categories?': participantCategory = defaultResponse('Do your participants belong to any of the following categories?'), //Do+your+participants+belong+to+any+of+the+following+categories?
    'What type of participants are you planning to use?': participantType = defaultResponse('What type of participants are you planning to use?'), //What+type+of+participants+are+you+planning+to+use?
    'If you are planning to use TVCs or minors, who is the pCounsel who has approved this study?': pcounsel = defaultResponse('If you are planning to use TVCs or minors, who is the pCounsel who has approved this study?'), //If+you+are+planning+to+use+TVCs+or+minors,+who+is+the+pCounsel+who+has+approved+this+study?
    'In which city/region/country will study sessions take place?': cityRegionCountryLocation = defaultResponse('In which city/region/country will study sessions take place?'), //In+which+city/region/country+will+study+sessions+take+place?
    'If you or another researcher received recruiting support for a previous study with similar criteria, please let us know the recruiter, study name, and approximate dates.': similarStudy = defaultResponse('If you or another researcher received recruiting support for a previous study with similar criteria, please let us know the recruiter, study name, and approximate dates.'), //If+you+or+another+researcher+received+recruiting+support+for+a+previous+study+with+similar+criteria,+please+let+us+know+the+recruiter,+study+name,+and+approximate+dates.
    'Paste the link to your Study Spreadsheet here. Ensure that your Screener Form has been linked to it and that the sharing settings for both are: private, with uxr-operations-share@google.com added as an editor/collaborator.': studySpreadsheetLink = defaultResponse('Paste the link to your Study Spreadsheet here. Ensure that your Screener Form has been linked to it and that the sharing settings for both are: private, with uxr-operations-share@google.com added as an editor/collaborator.'), //Paste+the+link+to+your+Study+Spreadsheet+here.+Ensure+that+your+Screener+Form+has+been+linked+to+it+and+that+the+sharing+settings+for+both+are:+private,+with+uxr-operations-share@google.com+added+as+an+editor/collaborator.
    'Please include a link to your research plan or any other relevant documentation that would be helpful for this recruitment effort.': researchPlan = defaultResponse('Please include a link to your research plan or any other relevant documentation that would be helpful for this recruitment effort.'), //Please+include+a+link+to+your+research+plan+or+any+other+relevant+documentation+that+would+be+helpful+for+this+recruitment+effort.
    "Anything else we should know? If you will be out of office or on vacation before your study begins, please indicate the dates and the primary contact person while you're gone.": otherData = defaultResponse("Anything else we should know? If you will be out of office or on vacation before your study begins, please indicate the dates and the primary contact person while you're gone."), //Anything+else+we+should+know?+If+you+will+be+out+of+office+or+on+vacation+before+your+study+begins,+please+indicate+the+dates+and+the+primary+contact+person+while+you're+gone.
    'Researcher Location': researcherLocation = defaultResponse('Researcher Location'), //Researcher+Location
    'Cost Center Code (recruiters do not edit this column)': costCenterCode = defaultResponse('Cost Center Code (recruiters do not edit this column)'), //Cost+Center+Code+(recruiters+do+not+edit+this+column)
    'Cost Center Name (recruiters do not edit this column)': costCenterName = defaultResponse('Cost Center Name (recruiters do not edit this column)'), //Cost+Center+Name+(recruiters+do+not+edit+this+column)
    'Product Area (recruiters do not edit this column)': productArea = defaultResponse('Product Area (recruiters do not edit this column)'), //Product+Area+(recruiters+do+not+edit+this+column)
    'Are they on the list of supported cost centers?': supported = defaultResponse('Are they on the list of supported cost centers?'), //Are+they+on+the+list+of+supported+cost+centers?
    'Pod': pod = defaultResponse('Pod'), //Pod
    'BU Code': buCode = defaultResponse('BU Code'), //BU+Code
    'Manager List': mangerList = defaultResponse('Manager List'), //Manager+List'
    'unified_rollup_level_1': unifiedRollup = defaultResponse('unified_rollup_level_1'), //unified_rollup_level_1
    'by ldap': fromLdap = defaultResponse('by ldap'), //by+ldap
    'from spreadsheet name': fromSpreadsheetName = defaultResponse('from spreadsheet name'), //from+spreadsheet+name
    'reason for transfer': transferReason = defaultResponse('reason for transfer'), //reason+for+transfer
    'spreadsheet id': spreadsheetId,
    // 'approve or deny': approvalStatus, //Approve || Deny

  } = namedValues
  const studyProps = Object.assign({}, { emailAddress: emailAddress, supportType: supportType, studyLocation: studyLocation, participantsNum: participantsNum, studyName: studyName, otherResearcher: otherResearcher, targetDate: targetDate, typeOfStudy: typeOfStudy, participantPreference: participantPreference, participantCategory: participantCategory, participantType: participantType, pcounsel: pcounsel, cityRegionCountryLocation: cityRegionCountryLocation, similarStudy: similarStudy, studySpreadsheetLink: studySpreadsheetLink, researchPlan: researchPlan, researcherLocation: researcherLocation, costCenterCode: costCenterCode, costCenterName: costCenterName, productArea: productArea, supported: supported, pod: pod, buCode, mangerList, unifiedRollup, fromLdap, fromSpreadsheetName, transferReason, otherData, spreadsheetId })
  const response = removeArrFromObjValue(studyProps)
  return response
  function removeArrFromObjValue(props) {
    for (const key in props) {
      let value = props[key]
      let newValue = Array.isArray(value) ? value[0] : value
      props[key] = newValue
    }
    return props
  }
}