// const devEmails = 'douglascox@google.com, skiggens@google.com ,uxi-mv-uat@google.com '
const sendToEmails = 'uxi-mv-uat@google.com'

function onFormSubmit(e) {
  Logger.log(JSON.stringify(e))
  e = e || artificalEvent();

  const htmlBody = getHtmlBody(e);
  const emailOptions = { htmlBody }
  Logger.log(`source: ${e.source}`)
  const spreadsheet = e.source
  SpreadsheetApp.getActiveSpreadsheet().getActiveSheet().getName()
  const isOnformSubmitBySheetName = ({ source }) => (sheetName) => sheetName === source.getActiveSheet().getName();
  const formSubmitController = onFormSubmitPath(e)
  if (formSubmitController.isNotification) {
    Logger.log('isNotification')
    approveDenyNotification(e)
  }
  if (formSubmitController.isApproveDeny) {
    Logger.log('isApproveDeny')
    approveDenyRouter(e)
  }
  function approveDenyRouter(e) {
    const studyqueueIds = {
      "194r-7nE90vmLSmq87ylU4DETJNg9wbYeKKa6OS8mr0Y": {            //spreadsheet Id
        Approve: "1DpWd_wVdbws2dfA-ocH6USTXuQhEOp-LrKLQhuHkGQk",   // formId
        Deny: "1K1SQf4qy_6RNhEPV2UFBrm99Rj8MHAwLsvhl_e55Vkc"       // formId
      },
      "1b1gf_VaeSEfxOK6q6GSthICcPglN5A78kVQUFIeSUQg": {          //spreadsheet id
        Approve: "1K1SQf4qy_6RNhEPV2UFBrm99Rj8MHAwLsvhl_e55Vkc", //form Id
        Deny: "1DpWd_wVdbws2dfA-ocH6USTXuQhEOp-LrKLQhuHkGQk"    //form ID
      }
    }
    const [fromSpreadsheetId] = e?.namedValues["spreadsheet id"]
    const [status] = e?.namedValues["approve or deny"];
    const inValidEntry = [status, fromSpreadsheetId].some(el => !el)
    if (inValidEntry) {
      return
    }
    const formId = studyqueueIds[fromSpreadsheetId][status]
    lockFNWrapper(moveToTransitQueue, formId, e)
  }

  function onFormSubmitPath(e) {
    const sheet = e?.range.getSheet();
    const isSheetName = (str) => sheet.getName() === str
    return {
      isNotification: isSheetName("Form Responses 2"),
      isApproveDeny: isSheetName("approve/deny")
    }
  }
}

function getHtmlBody({ namedValues }) {
  let studyProps = Object.entries(namedValues).reduce((acc, arr) => {
    let answer = arr[1][0] === "" ? "N/A" : arr[1][0]
    let question = arr[0]
    let formObj = { question, answer: answer };
    const validEntries = ["Cost Center Name (recruiters do not edit this column)", "reason for transfer", "What's the name of the study?", "What is the targeted first date of the study?"]
    return validEntries.includes(question) ? acc.concat(formObj) : acc
  }, [])
  return function (preFilledUrlLinks) {
    const { approveUrl, denyUrl } = preFilledUrlLinks
    studyProps.approveUrl = approveUrl
    studyProps.denyUrl = denyUrl
    return getHtmlBodyNotification(studyProps)

    function getHtmlBodyNotification(studyProps) {
      const htmlBody = HtmlService.createTemplateFromFile("notificationTemplate")
      htmlBody.data = studyProps
      return htmlBody.evaluate().getContent()
    }

  }
}
function approveDenyNotification(e) {
  e = e || artificalEvent()
  let studyProps = fetchSubmissionProps(e)
  let approvalmap = fetchApprovalFromQueryToHeaderMapping(studyProps);
  let final = getUrlLink(approvalmap)
  let fetchHtmlBody = compose(getHtmlBody(e), getUrlLink, fetchApprovalFromQueryToHeaderMapping, fetchSubmissionProps)
  const htmlBody = fetchHtmlBody(e)
  // Logger.log(JSON.stringify(preFilledUrlLinks.deny))
  try {
    GmailApp.sendEmail(sendToEmails, "Incoming Study Transfer Queue", htmlBody, { htmlBody })
  } catch (e) {
    throw (e)
  }
}