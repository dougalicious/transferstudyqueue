function artificalEvent(rowIdx) {
  const ss = SpreadsheetApp.getActiveSpreadsheet()
  const sheet = ss.getActiveSheet()
  const sheetName = sheet.getName()
  rowIdx = rowIdx || SpreadsheetApp.getActiveRange().getRow()
  const { body, headerObj } = getDataBySheet(sheet)
  const activeRowIdx = (!!rowIdx ? rowIdx : SpreadsheetApp.getActiveRange().getRow()) - 2
  const activeRowAsObj = body[activeRowIdx]
  const headers = Object.keys(activeRowAsObj)
  const values = Object.values(activeRowAsObj)
  for (let header in activeRowAsObj) {
    if (header !== undefined) {
      activeRowAsObj[header] = [activeRowAsObj[header]]
    }
  }

  const range = {
    getValues: () => Object.keys(activeRowAsObj).map((header) => activeRowAsObj[header][0]),
    getRow: () => activeRowIdx === -1 ? null : activeRowIdx,
    columnStart: 0,
    columnEnd: headers.length,
    getSheet: () => sheet,

  }
  const namedValues = { namedValues: activeRowAsObj, values }
  const v = range.getValues()

  
  return Object.assign(namedValues, { range });

  function getDataBySheet(sheet, headerIdx = 0, bodyRowStart = 1) {
    var dataRange = sheet.getDataRange();
    var values = dataRange.getValues()
    var headers = values.slice(headerIdx, headerIdx + 1)[0];
    var body = values.slice(bodyRowStart)
    var headerObj = headers.reduce(makeHeaderObj, {})
    var a = body.map(getValueByHeader)
    return {
      dataRange,
      body: body.map(getValueByHeader),
      headerObj,
      headers,
      getValueByHeader,
    }
    function makeHeaderObj(acc, next, idx) {
      if (next === "" || next === "-") {
        return acc;
      }
      if (acc.hasOwnProperty(next)) {
        throw (`Duplicate headers found ${next}`)
      }
      acc[next] = idx
      return acc;
    }
    // transform Array(row) => Object()
    function getValueByHeader(row, rowIdx) {
      var rowAsObject = Object.create(headerObj)
      for (var header in headerObj) {
        var idx = headerObj[header]
        rowAsObject[header] = row[idx]
      }
      Object.defineProperties(rowAsObject, {
        getRangeByColHeader: {
          value: ((header) => sheet.getRange(rowIdx + bodyRowStart + 1, headerObj[header] + 1)), // return range of column 4
          writable: false,
          enumerable: false,
        },
      })

      return rowAsObject
    }
  }
}

function compose(...fns){
  return x => fns.reduceRight((y, f) => f(y), x);
}

