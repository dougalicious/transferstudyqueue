function moveToTransitQueue(id, e) {
  const getLdap = (str) => str.slice(0, str.indexOf('@'))
  const [sessionEmail] = e?.namedValues["Email Address"] || ["Not Found"]
  const sessionLdap = getLdap(sessionEmail)
  const spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  isValidSelection(spreadsheet)
  formSubmitByActiveRange({ id, spreadsheet: e.range.getSheet(), cbArr: [setBackgroundColor] })
  
  // originally used for notification  
  function setRedirectLocation({ rowAsObject, status }) {
    // let a = rowAsObject.hasOwnProperty('getRange')
    // let range = status && rowAsObject.getRangeByHeader("outbound data");
    const form = FormApp.openById(id);
    const to = form.getDescription()
    let tsObject = { ts: new Date(), to, sessionLdap }
    const cellValue = Object.entries(tsObject)
    range.setValue(JSON.stringify(tsObject))
  }
  function setBackgroundColor({ rowAsObject, status, id }) {
    let setBackgroundColor = (color) => rowAsObject.getRangeOfRow().setBackgroundColor(color);
    const agencyObj ={
  "1DpWd_wVdbws2dfA-ocH6USTXuQhEOp-LrKLQhuHkGQk":"#a7c3de", //infosys
  "1K1SQf4qy_6RNhEPV2UFBrm99Rj8MHAwLsvhl_e55Vkc":"#d9ead3"  //answerlab 
}
    const color = agencyObj[id]
    status ? setBackgroundColor(color) : setBackgroundColor("#fce5cd");
  };

  function isValidSelection(spreadsheet) {
    const aboveFirstRow = (activeRange) => activeRange.getRow() > 1
    const singleActiveRange = (activeRange) => {
      let endRow = activeRange.getEndRow()
      let rowIdx = activeRange.getRow()
      if (rowIdx === endRow) {
        return true
      } else {
        throw (`Only works with single row selection 
        Request multi-selection update via @uxi-automation`)
      }
      // return false
    }
    return [aboveFirstRow, singleActiveRange].every(fn => fn(spreadsheet.getActiveRange())) ? true : (() => { throw ('InvalidSelection') })
  }
}

function formSubmitByActiveRange({ id, spreadsheet, cbArr }) {
  let { itemsList, formResponse, rowAsObject } = fetchQuestionAnswerFromTrix({ activeRange: spreadsheet.getActiveRange(), id })
  let isFormSubmit = setQuestionAnswerFromTrix({ itemsList, formResponse, rowAsObject });
  cbArr.forEach(cb => cb({ rowAsObject, status: isFormSubmit, id }))

  function setQuestionAnswerFromTrix({ itemsList, formResponse, rowAsObject }) {
    itemsList.forEach(populateForm);
    try {
      formResponse.submit();
      return true
    } catch (e) {
      thow(e)
      return false
    }

    function populateForm({ title, setFormResponse }) {
      let answer = rowAsObject.hasOwnProperty(title) ? rowAsObject[title] : null
      if (!!answer) {
        setFormResponse(answer);
        Logger.log(`${title} populated with ${answer}`);
        return true
      } else {
        return false
      }
    }
  }
  function fetchQuestionAnswerFromTrix({ activeRange, id }) {
    /** validation */
    // single row
    let rowAsObject = fetchActiveRangeAsObj(activeRange);
    // id = id || "1jcK76vqTrPpKva3zOtbkINhNxZlawJpsYWMNTG4fbvA"
    // let {itemsList, formResponse} = ;
    try {
      let transitForm = getTransitForm(id)
      return Object.assign({}, { rowAsObject, ...transitForm })
    }
    catch (e) {
      let email = Session.getActiveUser().getEmail()
      let atIdx = (str) => str.indexOf('@')
      let getLdap = (str) => str.slice(0, atIdx(str))
      let ldap = getLdap(email)
      throw (`${ldap} invalid permissions to move study out of studyqueue, \nplease reach out to uxi-automation@google.com`)
    }
  }
  function fetchActiveRangeAsObj(activeRange) {
    activeRange = activeRange || SpreadsheetApp.getActiveRange();
    let activeRow = activeRange.getRow();
    let sheet = activeRange.getSheet()
    let dataRange = sheet.getDataRange();
    let values = dataRange.getValues()
    let [header] = values;
    let row = values[activeRow - 1];
    let rowAsObj = convertToObj(header, row);
    let test = rowAsObj["Email"]

    Object.defineProperties(rowAsObj, {
      getRangeOfRow: {
        value: () => sheet.getRange(activeRow, 1, 1, sheet.getLastColumn()), // return range of column 4
        writable: false,
        enumerable: false,
      },
    })
    Object.defineProperties(rowAsObj, {
      getRangeByHeader: {
        value: (str) => {
          let headerIdx = header.indexOf(str)
          let idx = headerIdx !== -1 ? headerIdx + 1 : null
          let range = sheet.getRange(activeRow, idx)
          return range
        }, // return range of column 4
        writable: false,
        enumerable: false,
      },
    })
    return rowAsObj

    function convertToObj(headers, arr) {
      return headers.reduce((acc, next, idx) => {
        let cellAsObject = { [next]: arr[idx] }
        acc.hasOwnProperty(next) ? acc : Object.assign(acc, cellAsObject)
        return acc
      }, {})
    }
  }
  function getTransitForm(id) {
    const form = FormApp.openById(id)//('id')
    const formResponse = form.createResponse();
    const formItems = form.getItems();
    return {
      itemsList: getItems(formItems),
      formResponse
    }

    // let anon = questions.map(getItems)

    /*
     *
     * 
     *@return Object(..., setFormResponse)
     */
    function getItems(formItems) {
      return formItems.flatMap(item => {
        let title = item.getTitle();
        let itemType = item.getType().name();
        let question = item
        return {
          title, question, itemType, item,
          setFormResponse: setFormResponse({ question, itemType, formResponse })
        }

      })
    }

    function setFormResponse({ question, itemType, formResponse }) {
      return function (answer) {
        let itemOptions = {
          'TEXT': (answer) => formResponse.withItemResponse(question.asTextItem().createResponse(answer)),
          'PARAGRAPH_TEXT': (answer) => formResponse.withItemResponse(questions[n].asParagraphTextItem().createResponse(answer)),
          'DATE': (answer) => formResponse.withItemResponse(question.asDateItem().createResponse(answer)),
          'LIST': (answer) => formResponse.withItemResponse(question.asTextItem().createResponse(answer))
        }
        let _setItemResponse = itemOptions.hasOwnProperty(itemType) ? itemOptions[itemType] : null
        try {
          _setItemResponse(answer);
          return true;
        } catch (e) {
          throw (e);
          return false
        }
      }
    }

  }

}


