function onOpen() {
  var ui = SpreadsheetApp.getUi();
  // Or DocumentApp or FormApp.
  ui.createMenu('Options')
    .addItem('Resend accept/deny email', 'manualNotification')
    .addToUi();
}

function manualNotification() {
  // lockFNWrapper(moveToTransitQueue, formId, e)//(formId)
  const e = artificalEvent();
  lockFNWrapper(getManualNotification, e)

  function getManualNotification(e) {
    const formId = "1GhEshd4vTf0DZWPIYcfDTfnw7q-pLVKKZ1Z06qZe_l8"
    const htmlBody = compose(getHtmlBody(e), getUrlLink, fetchApprovalFromQueryToHeaderMapping, fetchSubmissionProps)(e);
    const sendEmail = Session.getUser().getEmail()
    GmailApp.sendEmail(sendEmail, "Incomming Study Transfer Queue", htmlBody, { htmlBody })
  }
}

function lockFNWrapper(fn, ...args) {
  const lock = LockService.getScriptLock()
  lock.waitLock(3000);
  try {
    fn(...args)
  } catch (e) {
    lock.releaseLock()
    throw (e)
  }
  lock.releaseLock()
}
